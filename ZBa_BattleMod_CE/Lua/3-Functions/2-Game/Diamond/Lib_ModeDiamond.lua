local B = CBW_Battle
local D = B.Diamond
local CV = B.Console
D.ID = nil
D.Spawns = {}

local rotatespd = ANG20
local diamondtext = "\x83".."Diamond".."\x80"

D.GameControl = function()
	if not(B.DiamondGametype())or not(#D.Spawns) or B.PreRoundWait() 
	then return end
	if D.ID == nil or not(D.ID.valid) then
		D.SpawnDiamond()
	end
end


D.Reset = function()
	if not(B.DiamondGametype()) then return end
	D.ID = nil
	D.Spawns = {}
	B.DebugPrint("Diamond mode reset",DF_GAMETYPE)
end

D.GetSpawns = function()
	if not(B.DiamondGametype()) then return end
	for thing in mapthings.iterate
		local t = thing.type
		if t == 3630 //Diamond Spawn object
			D.Spawns[#D.Spawns+1] = thing
			B.DebugPrint("Added Diamond spawn #"..#D.Spawns.. " from mapthing type "..t,DF_GAMETYPE)
		end
	end
	if not(#D.Spawns)
		B.DebugPrint("No diamond spawn points found on map. Checking for backup spawn positions...",DF_GAMETYPE)
		for thing in mapthings.iterate
			local t = thing.type
			if t == 1 //Player 1 Spawn
			or (t >= 330 and t <= 335) //Weapon Ring Panels
			or (t == 303) //Infinity Ring
			or (t == 3640) //Control Point
				D.Spawns[#D.Spawns+1] = thing
				B.DebugPrint("Added Diamond spawn #"..#D.Spawns.. " from mapthing type "..t,DF_GAMETYPE)
			end
		end
	end
end

local function free(mo)
	mo.fuse = TICRATE
	mo.flags = $&~MF_SPECIAL
	mo.idle = TICRATE*16
end

D.SpawnDiamond = function()
	B.DebugPrint("Attempting to spawn diamond",DF_GAMETYPE)
	local s = D.Spawns[P_RandomRange(1,#D.Spawns)]
	local fu = FRACUNIT
	local x = s.x*fu
	local y = s.y*fu
	local z = s.z*fu
	local subsector = R_PointInSubsector(x,y)
	if subsector.valid and subsector.sector then
		z = $+subsector.sector.ceilingheight
		D.ID = P_SpawnMobj(x,y,z,MT_DIAMOND)
		print("The "..diamondtext.." has been spawned!")
		B.DebugPrint("Diamond coordinates: "..D.ID.x/fu..","..D.ID.y/fu..","..D.ID.z/fu,DF_GAMETYPE)
	end
end

D.Collect = function(mo,toucher)
	if mo.target == toucher or not(toucher.player) then return end //This toucher has already collected the item, or is not a player
	if P_PlayerInPain(toucher.player) or toucher.player.powers[pw_flashing] then return end //Can't touch if we've recently taken damage
	if toucher.player.tossdelay then return end //Can't collect if tossflag is on cooldown
	local previoustarget = mo.target
	mo.target = toucher
	free(mo)
	mo.idle = nil
	S_StartSound(mo,sfx_lvpass)
	if not(previoustarget) then
		B.PrintGameFeed(toucher.player," picked up the "..diamondtext.."!")
	else
		B.PrintGameFeed(toucher.player," stole the "..diamondtext.." from ",previoustarget.player,"!")
	end
end

local points = function(player)
	if (B.Exiting) return end
	local p = 1
	P_AddPlayerScore(player,p)
	if G_GametypeHasTeams()
		if player.ctfteam == 1 then
			redscore = $+p
		else
			bluescore = $+p
		end
	end
end

D.Thinker = function(mo)
	mo.shadowscale = FRACUNIT>>2
	//Idle timer
	if mo.idle != nil then 
		mo.idle = $-1
		if mo.idle == 0
			P_SpawnMobj(mo.x,mo.y,mo.z,MT_SPARK)
			P_RemoveMobj(mo)
		return end
	end
	//Blink
	if mo.fuse&1
		mo.flags2 = $|MF2_DONTDRAW
	else
		mo.flags2 = $&~MF2_DONTDRAW
	end
	if mo.target then 
		mo.destscale = FRACUNIT
	else
		mo.destscale = FRACUNIT*2
	end
	
	//Sparkle
	if not(leveltime&3) then
		local i = P_SpawnMobj(mo.x,mo.y,mo.z-mo.height/4,MT_IVSP)
-- 		i.flags2 = $|MF2_SHADOW
		i.scale = mo.scale
		i.color = B.FlashRainbow(mo)
		i.colorized = true
		local g = P_SpawnGhostMobj(mo)
		g.color = B.FlashRainbow(mo)
		g.colorized = true
	end
	//Color
	mo.colorized = true	
	if not(mo.target) then
		mo.color = B.FlashColor(SKINCOLOR_SUPERSILVER1,SKINCOLOR_SUPERSILVER5)
	else
		mo.color = B.FlashRainbow(mo)
	end
	mo.angle = $+rotatespd
	for player in players.iterate
		if not player.mo then continue end
		if player.mo == mo.target then
			if player.cmd.buttons&BT_TOSSFLAG and not(player.tossdelay) then
				free(mo)
				mo.target = nil
				player.actioncooldown = TICRATE
				player.gotcrystal = false
				player.gotcrystal_time = 0
				P_TeleportMove(mo,player.mo.x,player.mo.y,player.mo.z)
				P_InstaThrust(mo,player.mo.angle,FRACUNIT*5)
				P_SetObjectMomZ(mo,FRACUNIT*10)
				player.tossdelay = TICRATE*2
			else
				points(player)
				player.gotcrystal = true
				player.gotcrystal_time = $ + 1
			end
		else
			player.gotcrystal = false
			player.gotcrystal_time = 0
		end
	end
	//Owner has been pushed by another player
	if mo.flags&MF_SPECIAL and mo.target and mo.target.valid 
	and mo.target.pushed_last and mo.target.pushed_last.valid
		D.Collect(mo,mo.target.pushed_last)
	end
	
	//Owner has taken damage or has gone missing
	if mo.target 
		if not(mo.target.valid)
		or P_PlayerInPain(mo.target.player)
		or mo.target.player.playerstate != PST_LIVE
		or mo.target.player.tossdelay
			if mo.target and mo.target.valid and mo.target.player then
				B.PrintGameFeed(mo.target.player," dropped the "..diamondtext..".")
			end
			mo.target = nil
			P_SetObjectMomZ(mo,FRACUNIT*10)
			P_InstaThrust(mo,mo.angle,FRACUNIT*5)	
			free(mo)
		end
	end
	
	//Unclaimed behavior
	if not(mo.target and mo.target.player) then
		mo.flags = ($|MF_BOUNCE)&~(MF_SLIDEME|MF_NOGRAVITY)
		if mo.z < mo.floorz+mo.scale*12 then
			mo.momz = $+mo.scale
		end
		return
	end
	
	//Claimed behavior
	mo.flags = ($&~MF_BOUNCE)|MF_NOGRAVITY|MF_SLIDEME
	local t = mo.target
	local ang = mo.angle + t.angle
	local dist = mo.target.radius*3
	local x = t.x+P_ReturnThrustX(mo,ang,dist)
	local y = t.y+P_ReturnThrustY(mo,ang,dist)
	local z = t.z+abs(leveltime&63-31)*FRACUNIT/2 //Gives us a hovering effect
	if P_MobjFlip(t) == 1 then //Make sure our vertical orientation is correct
		t.flags2 = $&~MF2_OBJECTFLIP
	else
		z = $+t.height
		t.flags2 = $|MF2_OBJECTFLIP
	end
	P_TeleportMove(mo,t.x,t.y,t.z)
-- 	P_TryMove(mo,x,y,true)
	P_InstaThrust(mo,R_PointToAngle2(mo.x,mo.y,x,y),min(FRACUNIT*60,R_PointToDist2(mo.x,mo.y,x,y)))
	mo.z = max(mo.floorz,min(mo.ceilingz+mo.height,z)) //Do z pos while respecting level geometry
	
	//Diamond capturing
	local player = t.player
	local captime = CV.DiamondCaptureTime.value * TICRATE
	if (player.gotcrystal_time == captime - 1 * TICRATE)
	or (player.gotcrystal_time == captime - 2 * TICRATE)
	or (player.gotcrystal_time == captime - 3 * TICRATE)
		S_StartSound(nil, sfx_s227) //Countdown sound effect
	end
	if player.gotcrystal_time >= captime
		player.gotcrystal_time = 0
		P_AddPlayerScore(player,CV.DiamondCaptureBonus.value)
		S_StartSound(nil, sfx_prloop)
		for p in players.iterate()
			if p == player or (G_GametypeHasTeams() and p.ctfteam == player.ctfteam) or p.spectator
				S_StartSound(nil, sfx_s3k68, p)
				continue
			end
			if G_GametypeHasTeams()
				S_StartSound(nil, sfx_lose, p)
				continue
			end
			S_StartSound(nil, sfx_s243, p)
		end
		P_RemoveMobj(mo)
		COM_BufInsertText(server, "csay "..player.name.."\\captured the "..diamondtext.."!\\\\")//Not sure how to color this text...
	end
end


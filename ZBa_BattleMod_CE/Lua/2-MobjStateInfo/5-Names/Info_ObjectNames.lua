//sonic
mobjinfo[MT_GROUNDPOUND].name = "ground pound splash"
//tails
mobjinfo[MT_SONICBOOM].name = "air cutter"
//knuckles
mobjinfo[MT_ROCKBLAST].name = "rock blast"
//amy
mobjinfo[MT_LHRT].name = "piko splash"
mobjinfo[MT_DUSTDEVIL].name = "piko tornado"
//fang
mobjinfo[MT_CORK].name = "popgun cork"
mobjinfo[MT_FBOMB].name = "bomb"
//metal sonic
mobjinfo[MT_ENERGYBLAST].name = "energy blast"
mobjinfo[MT_SLASH].name = "dash slicer claw"

//Other
mobjinfo[MT_JETTBULLET].name = "bullet"
